# Hints for Packing

Notes: Almost everything we have planned is a casual AKA you can wear, jeans/ shorts and a t-shirt. The one exception to this is our Toga Tuesday. The theme for the evening is (obviously) Toga Tuesday. Suggested attire- toga's and ancient Greek wear, tho we'll provide costumes so no worries.
**Please take note of the dress code for men for dinner**



### Nice to have Items
- "Gentlemen are kindly requested not to wear shorts or sleeveless shirts at dinner"
- Light rain jacket or poncho
- Swim suit or something you don't mind getting wet in- the hotel has a pool and a beach.
- Travel adapters & extention cords (label them with your name)
- Whatever you feel most confortable in.
- Something a bit dressier to wear to our casino night dinner
- Closed-toed, comfortable walking/hiking shoes are advisable for the Santorini trip and the afternoon to Heraklion 
- A hat
- Sandals or water shoes
- Sunglasses 
- Sunscreen and bug spay- always good to have some
- Any games you might like to play with the team on game night


### Recommended travel documents:
-  GitLab invitation email
   - Including hotel address
-  Printed copies of passport and visas
-  Flight tickets
-  Passport
-  Visa